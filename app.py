from flask import Flask, render_template

app = Flask(__name__)


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/identification/')
def identification():
    return render_template("template_IA.html")


@app.route('/projet/')
def present_project():
    return render_template("presentation_projet.html")


if __name__ == "__main__":
    # TODO Retirer debug=True quand le site sera terminé
    app.run(debug=True)
